package com.prcs251f.max.eventssystem;

import android.media.Image;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by Max on 01/03/2016.
 */
public class Event {

    private String eventId;
    private String eventTitle;
    private String eventDescription;
    private String eventImage;
    private String eventType;
    private String startTime;
    private String startDate;
    private String endTime;
    private String endDate;
    private String seatingPrice;
    private String standingPrice;
    private String venueId;

    public Event() {
        //Empty public constructor
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getSeatingPrice() {
        return seatingPrice;
    }

    public void setSeatingPrice(String seatingPrice) {
        this.seatingPrice = seatingPrice;
    }

    public String getStandingPrice() {
        return standingPrice;
    }

    public void setStandingPrice(String standingPrice) {
        this.standingPrice = standingPrice;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getVenueId() {
        return venueId;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }
}
