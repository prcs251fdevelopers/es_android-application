package com.prcs251f.max.eventssystem;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends ListFragment implements View.OnClickListener {

    private TextView noResultsText;
    private EditText toolbarSearch;
    private ImageButton searchButton;
    private String eventId;
    private String eventTitle;
    private String eventImage;
    private String venueId;
    private String eventStandingPrice;
    private String eventSeatingPrice;
    private String eventDescription;
    private String eventStartTime;
    private String eventStartDate;
    private ArrayList<Event> events = new ArrayList<>();
    private EventsAdapter eventsAdapter;
    private ProgressBar progressBarSearch;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        progressBarSearch = (ProgressBar) view.findViewById(R.id.progressBarSearch);
        noResultsText = (TextView) view.findViewById(R.id.searchResultsTextView);
        toolbarSearch = (EditText) getActivity().findViewById(R.id.search_toolbar);
        toolbarSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if(!toolbarSearch.getText().toString().trim().equals("")) {
                        noResultsText.setVisibility(View.GONE);
                        new SearchEvents().execute(toolbarSearch.getText().toString().trim().toUpperCase());
                    }
                    else {
                        events.clear();
                        eventsAdapter = new EventsAdapter(getActivity(), R.layout.event_row_item, events);
                        setListAdapter(eventsAdapter);
                        noResultsText.setVisibility(View.VISIBLE);
                        noResultsText.setText("We weren't able to find anything from nothing");
                    }
                    return true;
                }
                return false;
            }
        });
        searchButton = (ImageButton) getActivity().findViewById(R.id.toolbarSearchImageButton);

        return view;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if(getListView().getItemAtPosition(position) != null) {
            Event event = (Event) getListView().getItemAtPosition(position);
            eventId = event.getEventId();
            eventTitle = event.getEventTitle();
            eventStandingPrice = event.getStandingPrice();
            eventSeatingPrice = event.getSeatingPrice();
            eventImage = event.getEventImage();
            venueId = event.getVenueId();
            eventDescription = event.getEventDescription();
            eventStartTime = event.getStartTime();
            eventStartDate = event.getStartDate();
        }
        else {
            Toast.makeText(getContext(), "Item was null", Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent(this.getActivity(), EventDetailsActivity.class);
        Bundle b = new Bundle();
        b.putString("eventId", eventId);
        b.putString("eventTitle", eventTitle);
        b.putString("eventStandingPrice", eventStandingPrice);
        b.putString("eventSeatingPrice", eventSeatingPrice);
        b.putString("eventImage", eventImage);
        b.putString("venueId", venueId);
        b.putString("eventDescription", eventDescription);
        b.putString("eventStartTime", eventStartTime);
        b.putString("eventStartDate", eventStartDate);
        intent.putExtras(b);
        startActivity(intent);

    }

    @Override
    public void onClick(View v) {

    }

    private void closeKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void initSearchList(ArrayList<Event> events) {
        if(events.size() > 0 ) {
            eventsAdapter = new EventsAdapter(getActivity(), R.layout.event_row_item, events);
            setListAdapter(eventsAdapter);

            closeKeyboard();
        }
        else {
            if(eventsAdapter != null) {
                events.clear();
                eventsAdapter = new EventsAdapter(getActivity(), R.layout.event_row_item, events);
                setListAdapter(eventsAdapter);
            }
            noResultsText.setVisibility(View.VISIBLE);
            noResultsText.setText("No Results Found..Try another search");
        }
    }

    /**
     * Asynctask class to get the events on background thread - won't hold up the UI
     */
    class SearchEvents extends AsyncTask<String, Void, ArrayList<Event>> {

        @Override
        protected void onPreExecute() {
            progressBarSearch.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Event> doInBackground(String... params) {
            String eventToSearch = params[0];
            return searchEvents(eventToSearch);
        }

        @Override
        protected void onPostExecute(ArrayList<Event> events) {
            initSearchList(events);
            progressBarSearch.setVisibility(View.GONE);
            super.onPostExecute(events);
        }

        /**
         * Search Events Method
         */
        private ArrayList<Event> searchEvents(String eventToSearch) {
            ArrayList<Event> searchedEvents = new ArrayList<>();
            try {
                URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/event?name=" + eventToSearch);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                String next;
                while ((next = bufferedReader.readLine()) != null){
                    JSONArray ja = new JSONArray(next);

                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject jo = (JSONObject) ja.get(i);

                        Event event = new Event();
                        event.setEventId(jo.getString("EVENTID"));
                        event.setEventTitle(jo.getString("EVENTNAME"));
                        event.setStartTime(jo.getString("STARTTIME"));
                        event.setStartDate(jo.getString("STARTDATE"));
                        event.setEndTime(jo.getString("ENDTIME"));
                        event.setEndDate(jo.getString("ENDDATE"));
                        event.setEventType(jo.getString("EVENTTYPE"));
                        event.setSeatingPrice(jo.getString("SEATINGPRICE"));
                        event.setStandingPrice(jo.getString("STANDINGPRICE"));
                        event.setVenueId(jo.getString("VENUEID"));
                        event.setEventDescription(jo.getString("EVENTDESCRIPTION"));

                        //Random number generator for the pictures
                        Random random = new Random();
                        int randomNum = random.nextInt(9 - 1 + 1) + 1;

                        event.setEventImage(String.valueOf(randomNum));
                        searchedEvents.add(event);
                    }
                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return searchedEvents;
        }
    }

}
