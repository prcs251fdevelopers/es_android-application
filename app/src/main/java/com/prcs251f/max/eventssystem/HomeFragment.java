package com.prcs251f.max.eventssystem;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

public class HomeFragment extends ListFragment {

    private String eventId;
    private String eventTitle;
    private String eventStandingPrice;
    private String eventSeatingPrice;
    private String eventDescription;
    private String eventImage;
    private String eventStartTime;
    private String eventStartDate;
    private String venueId;
    private SwipeRefreshLayout mainSRL;
    private ProgressBar progressBarHome;
    private TextView connectionFailueText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final GetEvents getEvents = new GetEvents();

        progressBarHome = (ProgressBar) getActivity().findViewById(R.id.progressBarHome);
        connectionFailueText = (TextView) getActivity().findViewById(R.id.homeNoConnectionText);
        mainSRL = (SwipeRefreshLayout) getActivity().findViewById(R.id.homeSwipeRefresh);
        mainSRL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetEvents().execute();
                mainSRL.setRefreshing(false);
            }
        });

        getEvents.execute();
        View header = getLayoutInflater(savedInstanceState).inflate(R.layout.list_header_home, null);
        getListView().addHeaderView(header, null, false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if(getListView().getItemAtPosition(position) != null) {
            Event event = (Event) getListView().getItemAtPosition(position);
            eventId = event.getEventId();
            eventTitle = event.getEventTitle();
            eventStandingPrice = event.getStandingPrice();
            eventSeatingPrice = event.getSeatingPrice();
            eventImage = event.getEventImage();
            venueId = event.getVenueId();
            eventDescription = event.getEventDescription();
            eventStartTime = event.getStartTime();
            eventStartDate = event.getStartDate();
        }
        else {
            Toast.makeText(getContext(), "Item was null", Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent(this.getActivity(), EventDetailsActivity.class);
        Bundle b = new Bundle();
        b.putString("eventId", eventId);
        b.putString("eventTitle", eventTitle);
        b.putString("eventStandingPrice", eventStandingPrice);
        b.putString("eventSeatingPrice", eventSeatingPrice);
        b.putString("eventImage", eventImage);
        b.putString("venueId", venueId);
        b.putString("eventDescription", eventDescription);
        b.putString("eventStartTime", eventStartTime);
        b.putString("eventStartDate", eventStartDate);
        intent.putExtras(b);
        startActivity(intent);
    }
    
    //refreshing the list view on swipe refresh
    private void initEventList(ArrayList<Event> events) {
        EventsAdapter eventsAdapter;
        eventsAdapter = new EventsAdapter(getActivity(), R.layout.event_row_item, events);
        setListAdapter(eventsAdapter);
    }

    /**
     * Asynctask class to get the events on background thread - won't hold up the UI
     */
    class GetEvents extends AsyncTask<String, Void, ArrayList<Event>> {

        @Override
        protected void onPreExecute() {
            progressBarHome.setVisibility(View.VISIBLE);
            connectionFailueText.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Event> doInBackground(String... params) {
            return getAllEvents();
        }

        @Override
        protected void onPostExecute(ArrayList<Event> events) {
            initEventList(events);
            progressBarHome.setVisibility(View.GONE);
            if(events.size() <= 0) {
                connectionFailueText.setVisibility(View.VISIBLE);
            }
            else {
                connectionFailueText.setVisibility(View.GONE);
            }
            super.onPostExecute(events);
        }

        /**
         * Retrieve all existing events from the database via contacting the API
         * @return the events
         */
        private ArrayList<Event> getAllEvents() {
            ArrayList<Event> returnedEvents = new ArrayList<>();
            try {
                URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/event");

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                String next;
                while ((next = bufferedReader.readLine()) != null){
                    JSONArray ja = new JSONArray(next);

                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject jo = (JSONObject) ja.get(i);

                        Event event = new Event();
                        event.setEventId(jo.getString("EVENTID"));
                        event.setEventTitle(jo.getString("EVENTNAME"));
                        event.setStartTime(jo.getString("STARTTIME"));
                        event.setStartDate(jo.getString("STARTDATE"));
                        event.setEndTime(jo.getString("ENDTIME"));
                        event.setEndDate(jo.getString("ENDDATE"));
                        event.setEventType(jo.getString("EVENTTYPE"));
                        event.setSeatingPrice(jo.getString("SEATINGPRICE"));
                        event.setStandingPrice(jo.getString("STANDINGPRICE"));
                        event.setVenueId(jo.getString("VENUEID"));
                        event.setEventDescription(jo.getString("EVENTDESCRIPTION"));

                        //Random number generator for the pictures
                        Random random = new Random();
                        int randomNum = random.nextInt(9 - 1 + 1) + 1;

                        event.setEventImage(String.valueOf(randomNum));
                        returnedEvents.add(event);
                    }
                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return returnedEvents;
        }
    }

}
