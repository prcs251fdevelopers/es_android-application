package com.prcs251f.max.eventssystem;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;

public class SignUpActivity extends AppCompatActivity {

    EditText passwordText;
    EditText firstNameText;
    EditText lastNameText;
    EditText emailAddText;
    EditText phoneNoText;
    EditText addLineOneText;
    EditText postalCodeText;
    EditText dobEditText;
    ProgressBar signUpProgressBar;
    private String returnedUserId = "";
    private String returnedFirstName = "";
    SharedPreferences sharedPreferences;
    private LoginState loginState = new LoginState();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        sharedPreferences = getSharedPreferences("EventSystemPreferences", Context.MODE_PRIVATE);
        loginState.SetLogInState(sharedPreferences.getBoolean("LoginState", false));

        //Set up each input of the sign up
        signUpProgressBar = (ProgressBar) findViewById(R.id.progressBarSignUp);
        passwordText = (EditText) findViewById(R.id.passwordEditText);
        firstNameText = (EditText) findViewById(R.id.firstNameEditText);
        lastNameText = (EditText) findViewById(R.id.lastNameEditText);
        emailAddText = (EditText) findViewById(R.id.emailEditText);
        phoneNoText = (EditText) findViewById(R.id.phoneEditText);
        addLineOneText = (EditText) findViewById(R.id.addLine1);
        postalCodeText = (EditText) findViewById(R.id.postalCodeEditText);
        dobEditText = (EditText) findViewById(R.id.dobEditText);
        dobEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int slashCount = 0;
                char slash = "/".charAt(0);
                for (char c : dobEditText.getText().toString().toCharArray()) {
                    if(c == slash ) {
                        slashCount++;
                    }
                }

                if(slashCount != 2) {
                    if (dobEditText.getText().toString().length() == 2) {
                        dobEditText.append("/");
                    }
                    if (dobEditText.getText().toString().length() == 5) {
                        dobEditText.append("/");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void cancelSignUp(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void confirmSignUpLogIn(View view) {
        if(checkInputs()){
            new RegisterUser().execute();
        }
    }

    /**
     * The entered email address already exists
     */
    private void emailAlreadyExists() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage("That Email Address is already registered with our system")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Just Close
                    }
                });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    /**
     * Registration was successful login the new user
     */
    private void loginNewUser() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

        loginState.SetLogInState(true);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("LoginState", true);
        editor.putString("userId", returnedUserId);
        editor.putString("userFirstName", returnedFirstName);
        editor.apply();

        finish();
    }

    /**
     * Check if what the user has input is ok
     */
    private boolean checkInputs() {
        boolean emailOk = false;
        boolean passOk = false;
        boolean phoneOk = false;
        boolean firstOk = false;
        boolean lastOk = false;
        boolean addrOk = false;
        boolean postOk = false;
        boolean dobOk = false;

        if(emailAddText.getText().toString().trim().equals("")) {
            emailAddText.setHintTextColor(0xFFca4040);
        } else { emailOk = true; }
        if(passwordText.getText().toString().trim().equals("")) {
            passwordText.setHintTextColor(0xFFca4040);
        } else { passOk = true; }
        if(phoneNoText.getText().toString().trim().equals("")) {
            phoneNoText.setHintTextColor(0xFFca4040);
        } else { phoneOk = true; }
        if(firstNameText.getText().toString().trim().equals("")) {
            firstNameText.setHintTextColor(0xFFca4040);
        } else { firstOk = true; }
        if(lastNameText.getText().toString().trim().equals("")) {
            lastNameText.setHintTextColor(0xFFca4040);
        } else { lastOk = true; }
        if(addLineOneText.getText().toString().trim().equals("")) {
            addLineOneText.setHintTextColor(0xFFca4040);
        } else { addrOk = true; }
        if(postalCodeText.getText().toString().trim().equals("")) {
            postalCodeText.setHintTextColor(0xFFca4040);
        } else { postOk = true; }
        if(dobEditText.getText().toString().trim().equals("")) {
            dobEditText.setHintTextColor(0xFFca4040);
        } else { dobOk = true; }

        if(emailOk && passOk && phoneOk && firstOk && lastOk && addrOk && postOk && dobOk) {
            return true;
        }

        return false;
    }

    /**
     * Asynctask class to sign up a user to the application
     */
    class RegisterUser extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            signUpProgressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            return checkEmailExists();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            signUpProgressBar.setVisibility(View.GONE);

            if (result) {
                emailAlreadyExists();
            }
            else {
                loginNewUser();
            }

            super.onPostExecute(result);
        }

        private String hashPassword() {
            try {
                MessageDigest digest = MessageDigest.getInstance( "SHA-1" );
                byte[] bytes = passwordText.getText().toString().getBytes("UTF-8");
                digest.update(bytes, 0, bytes.length);
                bytes = digest.digest();

                char[] hexArray = "0123456789ABCDEF".toCharArray();
                char[] hexChars = new char[ bytes.length * 2 ];
                for( int j = 0; j < bytes.length; j++ )
                {
                    int v = bytes[ j ] & 0xFF;
                    hexChars[ j * 2 ] = hexArray[ v >>> 4 ];
                    hexChars[ j * 2 + 1 ] = hexArray[ v & 0x0F ];
                }
                return new String(hexChars);
            }
            catch (Exception e) {
                Log.e("No Algorithm", e.getMessage());
            }

            return "";
        }

        /**
         * Check if email address is registered
         */
        private boolean checkEmailExists() {
            String emailToCheck = emailAddText.getText().toString().toUpperCase();
            String returnedEmail = "";
            try {
                URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/Systemuser?email=" + emailToCheck);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                JSONObject jo = new JSONObject(new String(bufferedReader.readLine()));

                returnedEmail = jo.getString("EMAILADDRESS");
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            //return returnedEmail.equals(emailToCheck);
            if(!returnedEmail.equals(emailToCheck)) {
                String passwordHash = hashPassword();
                registerUser(passwordHash);
                return false;
            }
            return true;

        }

        /**
         * Create the user now
         */
        private void registerUser(String passwordHash) {
            String loginId = createNewLogin(passwordHash);
            String addressId = createNewAddress(addLineOneText.getText().toString().toUpperCase(),
                    postalCodeText.getText().toString().toUpperCase());

            //Now create the user
            try {
                URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/systemuser");
                String param = "ADDRESSID=" + URLEncoder.encode(addressId, "UTF-8") +
                        "&DOB=" + URLEncoder.encode(dobEditText.getText().toString(), "UTF-8") +
                        "&EMAILADDRESS=" + URLEncoder.encode(emailAddText.getText().toString().toUpperCase(), "UTF-8") +
                        "&FIRSTNAME=" + URLEncoder.encode(firstNameText.getText().toString().toUpperCase(), "UTF-8") +
                        "&LASTNAME=" + URLEncoder.encode(lastNameText.getText().toString().toUpperCase(), "UTF-8") +
                        "&LOGINID=" + URLEncoder.encode(loginId, "UTF-8") +
                        "&TELEPHONENUMBER=" + URLEncoder.encode(phoneNoText.getText().toString().toUpperCase(), "UTF-8") +
                        "&USERID=" + URLEncoder.encode("0", "UTF-8") +
                        "&USERTYPE=" + URLEncoder.encode("CUSTOMER", "UTF-8");

                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");

                conn.setFixedLengthStreamingMode(param.getBytes().length);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                PrintWriter out = new PrintWriter(conn.getOutputStream());
                out.print(param);
                out.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                JSONObject jo = new JSONObject(new String(bufferedReader.readLine()));

                returnedUserId = jo.getString("USERID");
                returnedFirstName = jo.getString("FIRSTNAME");
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * Create the user a new login
         */
        private String createNewLogin(String password) {
            try {
                //Finally create the user object
                URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/login");
                String param = "PASSWORD=" + URLEncoder.encode(password, "UTF-8");

                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");

                conn.setFixedLengthStreamingMode(param.getBytes().length);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                PrintWriter out = new PrintWriter(conn.getOutputStream());
                out.print(param);
                out.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                JSONObject jo = new JSONObject(new String(bufferedReader.readLine()));

                return jo.getString("LOGINID");

            }
            catch(Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        /**
         * Create the user a new address
         */
        private String createNewAddress(String addressLine, String postCode) {
            try {
                //Finally create the user object
                URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/address");
                String param = "ADDRESSLINE=" + URLEncoder.encode(addressLine, "UTF-8") +
                        "&POSTCODE=" + URLEncoder.encode(postCode, "UTF-8");

                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");

                conn.setFixedLengthStreamingMode(param.getBytes().length);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                PrintWriter out = new PrintWriter(conn.getOutputStream());
                out.print(param);
                out.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                JSONObject jo = new JSONObject(new String(bufferedReader.readLine()));

                return jo.getString("ADDRESSID");

            }
            catch(Exception e) {
                e.printStackTrace();
            }

            return "";
        }

    }
}
