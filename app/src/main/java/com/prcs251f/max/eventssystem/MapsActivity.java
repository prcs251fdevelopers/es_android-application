package com.prcs251f.max.eventssystem;

import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    TextView locationTitle;
    String eventAddress1;
    String city;
    String country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Toolbar Setup
        Toolbar toolbar = (Toolbar) findViewById(R.id.locationToolbar);
        locationTitle = (TextView) findViewById(R.id.locationTitleTextView);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Bundle b = getIntent().getExtras();
        String postalCode = b.getString("postalCode");
        String eventTitle = b.getString("eventTitle");
        locationTitle.setText(postalCode);

        LatLng address = convertPostalCode(postalCode);

        String eventDescription = eventAddress1 + "\n" + city + "\n" + country;

        //create a bigger window for the infoWindow
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                LinearLayout info = new LinearLayout(getBaseContext());
                info.setOrientation(LinearLayout.VERTICAL);
                info.setPadding(32, 32, 32, 32);

                info.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.popup_bg, null));

                TextView title = new TextView(getBaseContext());
                title.setTextColor(Color.WHITE);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());
                title.setPadding(16, 16, 16, 16);

                TextView snippet = new TextView(getBaseContext());
                snippet.setTextColor(Color.WHITE);
                snippet.setText(marker.getSnippet());
                snippet.setPadding(16, 0, 16, 16);

                info.addView(title);
                info.addView(snippet);

                return info;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });

        mMap.addMarker((new MarkerOptions().position(address).title(eventTitle).snippet(eventDescription))).showInfoWindow();

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(address, 15));
        Circle circle = mMap.addCircle(new CircleOptions()
                .center(address)
                .radius(100)
                .fillColor(0x30233E68)
                .strokeColor(0x301B2F4E)
                .strokeWidth(5));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
    }

    private LatLng convertPostalCode(String postalCode) {
        //convert a postal code into a lat and long variable to be used on the map
        //also take the full address
        LatLng convertedAddress = new LatLng(0, 0);
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        List<android.location.Address> address = null;

        try {
            address = geocoder.getFromLocationName(postalCode, 10);
        }
        catch (Exception ex) {
            Log.d("GEOCODER: ", "Unable to parse POSTALCODE " + ex.getMessage());
        }
        if(address != null && address.size() > 0) {
            android.location.Address firstAddress = address.get(0);
            eventAddress1 = firstAddress.getAddressLine(0);
            city = firstAddress.getLocality();
            country = firstAddress.getCountryName();
            double latitude = firstAddress.getLatitude();
            double longitude = firstAddress.getLongitude();
            convertedAddress = new LatLng(latitude, longitude);
        }
        return convertedAddress;
    }

}
