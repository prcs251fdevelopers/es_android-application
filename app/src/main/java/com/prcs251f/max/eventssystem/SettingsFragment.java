package com.prcs251f.max.eventssystem;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    private ListView settingsListView;
    private TextView userWelcomeText;
    private String userId;
    private String userFirstName;
    private ProgressBar progressBarSettings;
    SharedPreferences sharedPreferences;
    private View thisView;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        thisView = this.getView();
        progressBarSettings = (ProgressBar) getActivity().findViewById(R.id.progressBarSettings);
        settingsListView = (ListView) getActivity().findViewById(R.id.settingsListView);
        fillSettingsList();

        settingsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        new GetUserById().execute();
                        break;
                    case 1:
                        //Open booked tickets page
                        new GetTicketsForUser().execute();
                        break;
                }
            }
        });

        userWelcomeText = (TextView) getActivity().findViewById(R.id.userWelcomeTextView);
        sharedPreferences = getActivity().getSharedPreferences("EventSystemPreferences", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");
        userFirstName = sharedPreferences.getString("userFirstName", "").substring(0,1).toUpperCase() + sharedPreferences.getString("userFirstName", "").substring(1);
        userWelcomeText.setText("Welcome " + userFirstName + "!");

    }

    /**
     * Fill the settings list
     */
    private void fillSettingsList() {
        ArrayList<String> settingsItems = new ArrayList<>();
        settingsItems.add("Account Settings");
        settingsItems.add("Booked Tickets");
        ArrayAdapter settingsListAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, settingsItems);
        settingsListView.setAdapter(settingsListAdapter);
    }

    private void accountSettingsDialog(final User givenUser){
        final EditText firstNameEditText;
        final EditText lastNameEditText;
        final EditText emailAddressEditText;
        final EditText phoneNumberEditText;
        Button saveSettingsButton;

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_account_settings);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;

        firstNameEditText = (EditText) dialog.findViewById(R.id.dialogSettingsFirstNameEditText);
        lastNameEditText = (EditText) dialog.findViewById(R.id.dialogSettignsLastNameEditText);
        emailAddressEditText = (EditText) dialog.findViewById(R.id.dialogSettingsEmailEditText);
        phoneNumberEditText = (EditText) dialog.findViewById(R.id.dialogSettingsPhoneEditText);
        saveSettingsButton = (Button) dialog.findViewById(R.id.saveSettingsButton);
        saveSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //Update the user
                    givenUser.setFirstName(firstNameEditText.getText().toString().toUpperCase());
                    givenUser.setLastName(lastNameEditText.getText().toString().toUpperCase());
                    givenUser.setEmailAddress(emailAddressEditText.getText().toString().toUpperCase());
                    givenUser.setPhoneNumber(phoneNumberEditText.getText().toString());
                    new UpdateUser().execute(givenUser);

                    //Set their details again
                    userFirstName = givenUser.getFirstName().toLowerCase();
                    userFirstName = userFirstName.substring(0,1).toUpperCase() + userFirstName.substring(1);
                    userWelcomeText.setText("Welcome " + userFirstName + "!");
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("userFirstName", userFirstName);
                    editor.apply();

                    Toast.makeText(thisView.getContext(), "Details Successfully Updated", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }
        });

        firstNameEditText.setText(givenUser.getFirstName());
        lastNameEditText.setText(givenUser.getLastName());
        emailAddressEditText.setText(givenUser.getEmailAddress());
        phoneNumberEditText.setText(givenUser.getPhoneNumber());

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void accountTicketsDialog (ArrayList<Ticket> bookedTickets){
        ListView dialogListView;
        Button dialogOkButton;

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_booked_tickets);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;

        dialogListView = (ListView) dialog.findViewById(R.id.ticketsListView);
        ArrayList<String> bookedTicketText = new ArrayList<>();

        for (Ticket t : bookedTickets) {
            bookedTicketText.add(t.getTicketId() + " " + t.getTicketType());
        }

        ArrayAdapter settingsListAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, bookedTicketText);
        dialogListView.setAdapter(settingsListAdapter);

        dialogOkButton = (Button) dialog.findViewById(R.id.ticketsOkButton);
        dialogOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    /**
     * Asynctask class to update a user
     */
    class UpdateUser extends AsyncTask<User, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            progressBarSettings.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(User... params) {
            User theUser = params[0];
            return updateUserById(theUser);
        }

        @Override
        protected void onPostExecute(Boolean userCredentialsOK) {
            progressBarSettings.setVisibility(View.GONE);
            super.onPostExecute(userCredentialsOK);
        }

        /**
         * Update a user by their id
         */
        private boolean updateUserById(User givenUser) {
            //Put the user to the API
            try {
                URL url;
                HttpURLConnection conn;

                url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/systemuser");
                String param = "USERID=" + URLEncoder.encode(givenUser.getUserId(), "UTF-8") +
                        "&FIRSTNAME=" + URLEncoder.encode(givenUser.getFirstName(), "UTF-8") +
                        "&LASTNAME=" + URLEncoder.encode(givenUser.getLastName(), "UTF-8") +
                        "&EMAILADDRESS=" + URLEncoder.encode(givenUser.getEmailAddress(), "UTF-8") +
                        "&DOB=" + URLEncoder.encode(givenUser.getDOB(), "UTF-8") +
                        "&TELEPHONENUMBER=" + URLEncoder.encode(givenUser.getPhoneNumber(), "UTF-8") +
                        "&LOGINID=" + URLEncoder.encode(givenUser.getLoginId(), "UTF-8") +
                        "&ADDRESSID=" + URLEncoder.encode(givenUser.getAddressId(), "UTF-8") +
                        "&USERTYPE=" + URLEncoder.encode(givenUser.getUserType(), "UTF-8");

                conn = (HttpURLConnection)url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("PUT");

                conn.setFixedLengthStreamingMode(param.getBytes().length);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                PrintWriter out = new PrintWriter(conn.getOutputStream());
                out.print(param);
                out.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                String next = bufferedReader.readLine();
                Log.d("USER PUT RESPONSE--", next);
                return true;
            }
            catch(Exception e) {
                e.printStackTrace();
            }
            return false;
        }

    }

    /**
     * Asynctask class to get a user by id
     */
    class GetUserById extends AsyncTask<String, Void, User> {

        @Override
        protected void onPreExecute() {
            progressBarSettings.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected User doInBackground(String... params) {
            return getUserById(userId);
        }

        @Override
        protected void onPostExecute(User givenUser) {
            progressBarSettings.setVisibility(View.GONE);
            accountSettingsDialog(givenUser);
            super.onPostExecute(givenUser);
        }

        /**
         * Retrieve a user from an id
         * @return
         */
        private User getUserById(String userId) {
            User retrievedUser = new User();

            try {
                URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/systemuser?id=" + userId);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                JSONObject jo = new JSONObject(new String(bufferedReader.readLine()));

                retrievedUser.setUserId(jo.getString("USERID"));
                retrievedUser.setFirstName(jo.getString("FIRSTNAME"));
                retrievedUser.setLastName(jo.getString("LASTNAME"));
                retrievedUser.setEmailAddress(jo.getString("EMAILADDRESS"));
                retrievedUser.setPhoneNumber(jo.getString("TELEPHONENUMBER"));
                retrievedUser.setDOB(jo.getString("DOB"));
                retrievedUser.setAddressId(jo.getString("ADDRESSID"));
                retrievedUser.setLoginId(jo.getString("LOGINID"));
                retrievedUser.setUserType(jo.getString("USERTYPE"));

            }
            catch (Exception e) {
                e.printStackTrace();
            }

            return retrievedUser;
        }

    }

    /**
     * Asynctask class to get tickets for a user
     */
    class GetTicketsForUser extends AsyncTask<String, Void, ArrayList<Ticket>> {

        @Override
        protected void onPreExecute() {
            progressBarSettings.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Ticket> doInBackground(String... params) {
            return getTicketsByUser(userId);
        }

        @Override
        protected void onPostExecute(ArrayList<Ticket> userTickets) {
            progressBarSettings.setVisibility(View.GONE);
            accountTicketsDialog(userTickets);
            super.onPostExecute(userTickets);
        }

        /**
         * Retrieve a user from an id
         * @return
         */
        private ArrayList<Ticket> getTicketsByUser(String userId) {
            ArrayList<Ticket> tickets = new ArrayList<>();
            Ticket ticket;

            try {
                URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/ticket?id=" + userId);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                String next;
                while ((next = bufferedReader.readLine()) != null){
                    JSONArray ja = new JSONArray(next);

                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject jo = (JSONObject) ja.get(i);

                        ticket = new Ticket();
                        ticket.setUserId(jo.getString("USERID"));
                        ticket.setEventId(jo.getString("EVENTID"));
                        ticket.setTicketId(jo.getString("TICKETID"));
                        ticket.setTicketType(jo.getString("TICKETTYPE"));

                        tickets.add(ticket);
                    }
                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }

            return tickets;
        }

    }

}
