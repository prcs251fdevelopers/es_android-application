package com.prcs251f.max.eventssystem;

/**
 * Created by max on 09/05/2016.
 */
public class Venue {

    private String venueId;
    private String venueName;
    private String postCode;
    private String addressLine1;
    private String maximumCapacity;
    private String standingSpaces;
    private String seatingSpaces;

    public Venue () {
        //Empty public constructor
    }

    public String getVenueId() {
        return venueId;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getMaximumCapacity() {
        return maximumCapacity;
    }

    public void setMaximumCapacity(String maximumCapacity) {
        this.maximumCapacity = maximumCapacity;
    }

    public String getStandingSpaces() {
        return standingSpaces;
    }

    public void setStandingSpaces(String standingSpaces) {
        this.standingSpaces = standingSpaces;
    }

    public String getSeatingSpaces() {
        return seatingSpaces;
    }

    public void setSeatingSpaces(String seatingSpaces) {
        this.seatingSpaces = seatingSpaces;
    }
}
