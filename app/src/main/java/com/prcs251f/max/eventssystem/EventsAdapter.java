package com.prcs251f.max.eventssystem;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Max on 11/04/2016.
 */
public class EventsAdapter extends ArrayAdapter<Event> {

    private ArrayList<Event> events = new ArrayList<Event>();

    public EventsAdapter(Context context, int textViewResourceId, ArrayList<Event> events) {
        super(context, textViewResourceId, events);
        this.events = events;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.event_row_item, null);
        }

        Event event = events.get(position);
        if (event != null) {
            TextView eventTitle = (TextView) view.findViewById(R.id.eventNameTextView);
            TextView eventPrice = (TextView) view.findViewById(R.id.eventPriceTextView);
            TextView eventTime = (TextView) view.findViewById(R.id.eventDateTextView);
            ImageView eventImage = (ImageView) view.findViewById(R.id.rowImageView);

            if(eventTitle != null) {
                eventTitle.setText(event.getEventTitle());
            }

            if(eventPrice != null) {
                eventPrice.setText(event.getSeatingPrice());
            }

            if(eventTime != null) {
                eventTime.setText(event.getStartDate() + " " + event.getStartTime());
            }

            if(eventImage != null) {
                if(Integer.parseInt(event.getEventImage()) == 1) {
                    eventImage.setImageDrawable(ResourcesCompat.getDrawable(view.getResources(), R.drawable.event_image, null));
                }
                else if(Integer.parseInt(event.getEventImage()) == 2) {
                    eventImage.setImageDrawable(ResourcesCompat.getDrawable(view.getResources(), R.drawable.event_image_2, null));
                }
                else if (Integer.parseInt(event.getEventImage()) == 3) {
                    eventImage.setImageDrawable(ResourcesCompat.getDrawable(view.getResources(), R.drawable.event_image_3, null));
                }
                else if (Integer.parseInt(event.getEventImage()) == 4) {
                    eventImage.setImageDrawable(ResourcesCompat.getDrawable(view.getResources(), R.drawable.event_image_4, null));
                }
                else if (Integer.parseInt(event.getEventImage()) == 5) {
                    eventImage.setImageDrawable(ResourcesCompat.getDrawable(view.getResources(), R.drawable.event_image_5, null));
                }
                else if (Integer.parseInt(event.getEventImage()) == 6) {
                    eventImage.setImageDrawable(ResourcesCompat.getDrawable(view.getResources(), R.drawable.event_image_6, null));
                }
                else if (Integer.parseInt(event.getEventImage()) == 7) {
                    eventImage.setImageDrawable(ResourcesCompat.getDrawable(view.getResources(), R.drawable.event_image_7, null));
                }
                else if (Integer.parseInt(event.getEventImage()) == 8) {
                    eventImage.setImageDrawable(ResourcesCompat.getDrawable(view.getResources(), R.drawable.event_image_8, null));
                }
                else if (Integer.parseInt(event.getEventImage()) == 9) {
                    eventImage.setImageDrawable(ResourcesCompat.getDrawable(view.getResources(), R.drawable.event_image_9, null));
                }
                else {
                    eventImage.setImageDrawable(ResourcesCompat.getDrawable(view.getResources(), R.drawable.event_image, null));
                }
            }

        }

        // the view must be returned to our activity
        return view;
    }
}
