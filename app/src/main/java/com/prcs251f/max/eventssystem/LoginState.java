package com.prcs251f.max.eventssystem;

/**
 * Created by Max on 01/03/2016.
 */
public class LoginState {

    private boolean loggedIn = false;
    private String username;
    private String password;

    public LoginState() {

    }

    public void SetLogInState(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public boolean GetLogInState() {
        return this.loggedIn;
    }

    public void SetLocalUsername(String username) {
        this.username = username;
    }

    public String GetUsername() {
        return this.username;
    }

    public void SetPassword(String password) {
        this.password = password;
    }

    public String GetPassword() {
        return this.password;
    }
}
