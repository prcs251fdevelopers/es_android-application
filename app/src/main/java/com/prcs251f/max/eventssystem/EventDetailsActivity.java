package com.prcs251f.max.eventssystem;

import android.app.Dialog;
import android.content.Intent;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class EventDetailsActivity extends AppCompatActivity {

    private TextView eventTitleTextView;
    private TextView detailsPriceTextView;
    private TextView eventDescriptionTextView;
    private ImageView eventImageView;
    private TextView eventTimesTextView;
    private String eventId;
    private String eventTitle;
    private String eventStandingPrice;
    private String eventSeatingPrice;
    private String venueId;
    private String eventDescription;
    private String eventTimes;
    private int eventImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        //Toolbar Setup
        Toolbar toolbar = (Toolbar) findViewById(R.id.eventDetailsToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //event properties setup
        Bundle b = getIntent().getExtras();
        eventTitleTextView = (TextView) findViewById(R.id.eventTitleTextView);
        eventTimesTextView = (TextView) findViewById(R.id.eventTimeTextView);
        eventDescriptionTextView = (TextView) findViewById(R.id.eventDescriptionTextView);
        eventId = b.getString("eventId");
        eventTitle = b.getString("eventTitle");
        eventImage = Integer.parseInt(b.getString("eventImage"));
        eventStandingPrice = b.getString("eventStandingPrice");
        eventSeatingPrice = b.getString("eventSeatingPrice");
        venueId = b.getString("venueId");
        eventDescription = b.getString("eventDescription");
        eventTimes = b.getString("eventStartDate") + " " + b.getString("eventStartTime");

        eventTitleTextView.setText(eventTitle);
        eventDescriptionTextView.setText(eventDescription);
        eventTimesTextView.setText(eventTimes);
        detailsPriceTextView = (TextView) findViewById(R.id.bookingPriceText);
        detailsPriceTextView.setText("Starting from " + eventSeatingPrice + " each ticket");
        eventImageView = (ImageView) findViewById(R.id.eventDetailImage);
        if(eventImage > 0) {
            if(eventImage == 1) {
                eventImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.event_image, null));
            }
            else if(eventImage == 2) {
                eventImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.event_image_2, null));
            }
            else if (eventImage == 3) {
                eventImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.event_image_3, null));
            }
            else if (eventImage == 4) {
                eventImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.event_image_4, null));
            }
            else if (eventImage == 5) {
                eventImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.event_image_5, null));
            }
            else if (eventImage == 6) {
                eventImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.event_image_6, null));
            }
            else if (eventImage == 7) {
                eventImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.event_image_7, null));
            }
            else if (eventImage == 8) {
                eventImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.event_image_8, null));
            }
            else if (eventImage == 9) {
                eventImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.event_image_9, null));
            }
            else {
                eventImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.event_image, null));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Invoke the ticket booking activity
     * @param view
     */
    public void bookTicket(View view) {
        Intent intent = new Intent(this, BookTicketActivity.class);
        Bundle b = new Bundle();
        b.putString("eventTitle", eventTitle + " Booking Page");
        b.putString("eventStandingPrice", eventStandingPrice);
        b.putString("eventSeatingPrice", eventSeatingPrice);
        b.putString("eventTimes", eventTimes);
        b.putString("eventId", eventId);
        intent.putExtras(b);
        startActivity(intent);
    }

    /**
     * Invoke the maps activity to display an events location
     * @param view
     */
    public void showMap(View view) {
        Intent intent = new Intent(this, MapsActivity.class);

        ApiVenueHandler apiVenueHandler = new ApiVenueHandler();
        Venue returnedVenue = new Venue();
        try {
            returnedVenue = apiVenueHandler.execute("getVenue", venueId).get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String returnedVenuePostCode = "OX14 4LS";
        if(returnedVenue != null) {
            returnedVenuePostCode = returnedVenue.getPostCode();
        }

        Bundle b = new Bundle();
        b.putString("eventTitle", eventTitle);
        b.putString("postalCode", returnedVenuePostCode);
        intent.putExtras(b);
        startActivity(intent);
    }

}
