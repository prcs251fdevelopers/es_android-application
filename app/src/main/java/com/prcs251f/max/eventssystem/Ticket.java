package com.prcs251f.max.eventssystem;

/**
 * Created by max on 15/05/2016.
 */
public class Ticket {

    private String eventId;
    private String userId;
    private String ticketId;
    private String ticketType;

    public Ticket () {
        //Empty public constructor
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }
}
