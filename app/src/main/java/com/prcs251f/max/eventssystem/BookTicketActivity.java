package com.prcs251f.max.eventssystem;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Random;

public class BookTicketActivity extends AppCompatActivity {

    private Toolbar bookingToolbar;
    private String eventTitle;
    private TextView bookingTitleText;
    private Spinner eventTimeSpinner;
    private Spinner seatingAmountSpinner;
    private Spinner standingAmountSpinner;
    private TextView totalTicketPriceTextView;
    private TextView seatingPriceTextView;
    private TextView standingPriceTextView;
    private String eventTimes;
    private String eventId;
    private String userId;

    private int currentStandingNumber = 0;
    private int totalTicketPrice = 0;
    private int currentSeatingNumber = 0;
    private int standingPrice = 0;
    private int seatingPrice = 0;
    private SharedPreferences sharedPreferences;
    private ProgressBar progressBarBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_ticket);
        initToolbar();

        standingPriceTextView = (TextView) findViewById(R.id.seatingOnePriceText);
        seatingPriceTextView = (TextView) findViewById(R.id.seatingTwoPriceText);
        eventTimeSpinner = (Spinner) findViewById(R.id.bookingTimeSpinner);
        standingAmountSpinner = (Spinner) findViewById(R.id.bookingStandingSpinner);
        seatingAmountSpinner = (Spinner) findViewById(R.id.bookingSeatedSpinner);
        progressBarBook = (ProgressBar) findViewById(R.id.progressBarBook);

        sharedPreferences = getSharedPreferences("EventSystemPreferences", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("userId", "");

        //Retrieve the event title + anything extra needed
        Bundle b = getIntent().getExtras();
        eventId = b.getString("eventId");
        eventTitle = b.getString("eventTitle");
        eventTimes = b.getString("eventTimes");
        seatingPrice = Integer.parseInt(b.getString("eventSeatingPrice").substring(1));
        standingPrice = Integer.parseInt(b.getString("eventStandingPrice").substring(1));

        standingPriceTextView.setText(b.getString("eventStandingPrice"));
        seatingPriceTextView.setText(b.getString("eventSeatingPrice"));

        bookingTitleText = (TextView) findViewById(R.id.bookingTitleTextView);
        if (bookingTitleText != null) {
            bookingTitleText.setText(eventTitle);
        }

        initSpinners();
    }

    private void initToolbar() {
        bookingToolbar = (Toolbar) findViewById(R.id.bookingToolbar);
        setSupportActionBar(bookingToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void confirmPayment(View view) {
        if(standingAmountSpinner.getSelectedItemPosition() > 0 || seatingAmountSpinner.getSelectedItemPosition() > 0) {
            ArrayList<Ticket> ticketsToBook = new ArrayList<>();
            if(standingAmountSpinner.getSelectedItemPosition() > 0) {
                Ticket standingTicket;
                for (int i = 0; i < standingAmountSpinner.getSelectedItemPosition(); i++) {
                    standingTicket = new Ticket();
                    standingTicket.setEventId(eventId);
                    standingTicket.setTicketId("0");
                    standingTicket.setTicketType("STANDING");
                    standingTicket.setUserId(userId);
                    ticketsToBook.add(standingTicket);
                }
            }
            if (seatingAmountSpinner.getSelectedItemPosition() > 0) {
                Ticket seatingTicket;
                for (int i = 0; i < seatingAmountSpinner.getSelectedItemPosition(); i++) {
                    seatingTicket = new Ticket();
                    seatingTicket.setEventId(eventId);
                    seatingTicket.setTicketId("0");
                    seatingTicket.setTicketType("SEATING");
                    seatingTicket.setUserId(userId);
                    ticketsToBook.add(seatingTicket);
                }
            }

            new BookTicket().execute(ticketsToBook);
        }
        else {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setMessage("Please select a number of tickets to book")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //Just Close
                        }
                    });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }
    }

    private void confirmTickets() {
        finish();
        Toast.makeText(getBaseContext(), "Tickets Successfully booked", Toast.LENGTH_LONG).show();
    }

    /**
     * Initialization of the spinner objects in the booking ticket Activity
     */
    private void initSpinners() {
        totalTicketPriceTextView = (TextView) findViewById(R.id.bookingPriceText);
        ArrayList<String> times = new ArrayList<>();
        times.add(eventTimes);

        ArrayAdapter<String> timeDataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, times);
        timeDataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_items);
        eventTimeSpinner.setAdapter(timeDataAdapter);

        ArrayList<String> ticketAmounts = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ticketAmounts.add(String.valueOf(i));
        }
        ArrayAdapter<String> ticketAmountDataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, ticketAmounts);
        ticketAmountDataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_items);
        standingAmountSpinner.setAdapter(ticketAmountDataAdapter);
        standingAmountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentStandingNumber = position;
                totalTicketPrice = (currentStandingNumber * standingPrice) + (currentSeatingNumber * seatingPrice);
                totalTicketPriceTextView.setText("£" + String.valueOf(totalTicketPrice));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        seatingAmountSpinner.setAdapter(ticketAmountDataAdapter);
        seatingAmountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentSeatingNumber = position;
                totalTicketPrice = (currentStandingNumber * standingPrice) + (currentSeatingNumber * seatingPrice);
                totalTicketPriceTextView.setText("£" + String.valueOf(totalTicketPrice));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        totalTicketPriceTextView.setText("£" + String.valueOf(totalTicketPrice));
    }

    /**
     * Asynctask class to post tickets
     */
    class BookTicket extends AsyncTask<ArrayList<Ticket>, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            progressBarBook.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(ArrayList<Ticket>... params) {
            ArrayList<Ticket> tickets = params[0];
            postNewTicket(tickets);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            progressBarBook.setVisibility(View.GONE);
            super.onPostExecute(aBoolean);
            confirmTickets();
        }

        /**
         * Post a new ticket
         */
        private void postNewTicket (ArrayList<Ticket> ticketsToPost) {
            for (Ticket t : ticketsToPost) {
                try {
                    URL url;
                    HttpURLConnection conn;

                    url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/ticket");
                    String param = "TICKETID=" + URLEncoder.encode(t.getTicketId(), "UTF-8") +
                            "&TICKETTYPE=" + URLEncoder.encode(t.getTicketType(), "UTF-8") +
                            "&EVENTID=" + URLEncoder.encode(t.getEventId(), "UTF-8") +
                            "&USERID=" + URLEncoder.encode(t.getUserId(), "UTF-8");

                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");

                    conn.setFixedLengthStreamingMode(param.getBytes().length);
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                    PrintWriter out = new PrintWriter(conn.getOutputStream());
                    out.print(param);
                    out.close();

                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    String next = bufferedReader.readLine();
                    Log.d("LOGIN--", next);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

    }
}
