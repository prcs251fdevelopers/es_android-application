package com.prcs251f.max.eventssystem;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView toolbarTitle;
    ImageButton filterIcon;
    EditText toolbarSearch;
    SharedPreferences sharedPreferences;
    View clearView;
    ImageButton clearSearchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get shared preferences
        sharedPreferences = getSharedPreferences("EventSystemPreferences", Context.MODE_PRIVATE);

        //Toolbar Setup
        toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        filterIcon = (ImageButton) findViewById(R.id.filterIcon);
        toolbarSearch = (EditText) findViewById(R.id.search_toolbar);
        clearView = findViewById(R.id.searchClearView);
        clearSearchButton = (ImageButton) findViewById(R.id.clearSearchButton);


        //control the clear text button on the search
        toolbarSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(toolbarSearch.getText().toString().trim().length() > 0) {
                    clearView.setVisibility(View.GONE);
                    clearSearchButton.setVisibility(View.VISIBLE);
                }
                else {
                    clearView.setVisibility(View.VISIBLE);
                    clearSearchButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //Tab bar set up
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        if(tabLayout != null) {
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_action_search_white));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_action_home_white));
            tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_action_settings_white));

            tabLayout.getTabAt(0).setCustomView(R.layout.tab_sizes);
            tabLayout.getTabAt(1).setCustomView(R.layout.tab_sizes);
            tabLayout.getTabAt(2).setCustomView(R.layout.tab_sizes);
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        }


        //Viewpager setup
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        //onTabSelection controls
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                setTabChanges(tab.getPosition(), tabLayout, toolbarTitle);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //set homepage on start up
        viewPager.setCurrentItem(1);

    }

    /**
     * Set Tab changes
     * @param tabNumber
     * @param tabLayout
     * @param toolbarTitle
     */
    public void setTabChanges(int tabNumber, TabLayout tabLayout, TextView toolbarTitle) {
        switch(tabNumber) {
            case 0:
                tabLayout.getTabAt(0).setIcon(R.drawable.ic_action_search_cyan);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_action_home_white);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_action_settings_white);
                filterIcon.setVisibility(View.VISIBLE);
                toolbarTitle.setVisibility(View.GONE);
                toolbarSearch.setVisibility(View.VISIBLE);
                if(toolbarSearch.getText().toString().trim().length() > 0) {
                    clearView.setVisibility(View.GONE);
                    clearSearchButton.setVisibility(View.VISIBLE);
                }
                else {
                    clearView.setVisibility(View.VISIBLE);
                    clearSearchButton.setVisibility(View.GONE);
                }

                break;
            case 1:
                closeKeyboard();
                tabLayout.getTabAt(0).setIcon(R.drawable.ic_action_search_white);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_action_home_cyan);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_action_settings_white);
                filterIcon.setVisibility(View.INVISIBLE);
                toolbarTitle.setText("Home");
                toolbarTitle.setVisibility(View.VISIBLE);
                toolbarSearch.setVisibility(View.GONE);
                clearView.setVisibility(View.GONE);
                clearSearchButton.setVisibility(View.GONE);
                break;
            case 2:
                closeKeyboard();
                tabLayout.getTabAt(0).setIcon(R.drawable.ic_action_search_white);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_action_home_white);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_action_settings_cyan);
                filterIcon.setVisibility(View.INVISIBLE);
                toolbarTitle.setText("Settings");
                toolbarTitle.setVisibility(View.VISIBLE);
                toolbarSearch.setVisibility(View.GONE);
                clearView.setVisibility(View.GONE);
                clearSearchButton.setVisibility(View.GONE);
                break;
        }
    }

    public void logOut(View view) {
        //now remove the shared preferences reference
        sharedPreferences.edit().putBoolean("LoginState", false).apply();
        sharedPreferences.edit().putString("userId", "").apply();
        sharedPreferences.edit().putString("userFirstName", "").apply();

        //end main activity
        finish();

        //restart the login screen
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void filterDialog(View view) {
        Button filterButton;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_filter);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        filterButton = (Button) dialog.findViewById(R.id.resetButton);
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void clearSearchText(View view) {
        toolbarSearch.setText("");
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
