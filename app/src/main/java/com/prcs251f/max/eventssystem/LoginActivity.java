package com.prcs251f.max.eventssystem;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Random;

public class LoginActivity extends AppCompatActivity {

    EditText usernameEditText;
    EditText passwordEditText;
    private String email;
    private String password;
    private String userId;
    private String userFirstName;
    String esPreferences = "EventSystemPreferences";
    SharedPreferences sharedPreferences;
    ProgressBar loginProgress;

    private LoginState loginState = new LoginState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //input property set
        usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        loginProgress = (ProgressBar) findViewById(R.id.progressBarLogin);

        sharedPreferences = getSharedPreferences(esPreferences, Context.MODE_PRIVATE);
        loginState.SetLogInState(sharedPreferences.getBoolean("LoginState", false));

        if(loginState.GetLogInState()) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);

            finish();
        }
    }

    /**
     * Credentials were okay so login this user
     */
    private void loginUser() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

        loginState.SetLogInState(true);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("LoginState", true);
        editor.putString("userId", userId);
        editor.putString("userFirstName", userFirstName.toLowerCase());
        editor.apply();

        finish();
    }

    /**
     * Users' details were wrong show dialog telling them so
     */
    private void wrongDetails() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage("Email or Password was incorrect")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Just Close
                    }
                });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public void loginCheck(View view) {
        email = usernameEditText.getText().toString().toUpperCase();
        password = passwordEditText.getText().toString();
        new LoginUser().execute();
        closeKeyboard();
    }

    /**
     * Method to close the keyboard
     */
    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Sign up logic
     * @param view
     */
    public void signUp(View view) {
        //Set inputs
        usernameEditText.setText("");
        passwordEditText.setText("");

        //Start sign up activity
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);

        //finish login activity so it's not lost in the background
        finish();
    }

    /**
     * Forgotten password dialog handler
     * @param view
     */
    public void forgotPasswordDialog(View view) {
        Button resetButton;
        EditText emailAddressEditText;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_forgot_password);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        emailAddressEditText = (EditText) dialog.findViewById(R.id.emailAddressEditText);
        final String emailAddress = emailAddressEditText.getText().toString();

        resetButton = (Button) dialog.findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResetEmail(emailAddress);
                dialog.dismiss();
            }
        });
    }

    /**
     * Method to send an email to the user if they've forgotton their username or password
     * @param emailAddress
     * @return
     */
    private boolean sendResetEmail(String emailAddress) {
        boolean emailSentSuccess = false;

        return emailSentSuccess;
    }

    /**
     * Asynctask class to login a user to the application
     */
    class LoginUser extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            loginProgress.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            hashPassword();
            return checkLoginDetails(email, password);
        }

        @Override
        protected void onPostExecute(Boolean userCredentialsOK) {
            loginProgress.setVisibility(View.GONE);
            if(userCredentialsOK) {
                loginUser();
            }
            else {
                wrongDetails();
            }
            super.onPostExecute(userCredentialsOK);
        }

        private void hashPassword() {
            try {
                MessageDigest digest = MessageDigest.getInstance( "SHA-1" );
                byte[] bytes = password.getBytes("UTF-8");
                digest.update(bytes, 0, bytes.length);
                bytes = digest.digest();

                char[] hexArray = "0123456789ABCDEF".toCharArray();
                char[] hexChars = new char[ bytes.length * 2 ];
                for( int j = 0; j < bytes.length; j++ )
                {
                    int v = bytes[ j ] & 0xFF;
                    hexChars[ j * 2 ] = hexArray[ v >>> 4 ];
                    hexChars[ j * 2 + 1 ] = hexArray[ v & 0x0F ];
                }
                password = new String(hexChars);
            }
            catch (Exception e) {
                Log.e("No Algorithm", e.getMessage());
            }
        }

        /**
         * Method to check a users' login details
         * @param email
         * @param password
         * @return
         */
        private boolean checkLoginDetails(String email, String password) {
            String returnedLoginId = "";

            //Get user id from email address
            returnedLoginId = getUserLoginId(email);

            //Get login object from user id
            if(!returnedLoginId.equals("")) {
                if(password.equals(getLoginPassword(returnedLoginId))) {
                    return true;
                }
            }

            return false;
        }

        /**
         * Method to return a user id based on their email address
         * @return
         */
        private String getUserLoginId(String email) {
            String returnedId = "";

            //Logic to get a user id from their email
            try {
                URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/Systemuser?email=" + email);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                JSONObject jo = new JSONObject(new String(bufferedReader.readLine()));

                userId = jo.getString("USERID");
                userFirstName = jo.getString("FIRSTNAME");
                returnedId = jo.getString("LOGINID");
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            return returnedId;
        }

        /**
         * Method to return a password based on a users' id
         * @return
         */
        private String getLoginPassword(String loginId) {
            String returnedPassword = "";

            //Logic to get a password from a user id
            try {
                URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/Login?loginId=" + loginId);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                JSONObject jo = new JSONObject(new String(bufferedReader.readLine()));

                returnedPassword = jo.getString("PASSWORD");
            } catch (Exception e) {
                e.printStackTrace();
            }

            return returnedPassword;
        }

    }
}
