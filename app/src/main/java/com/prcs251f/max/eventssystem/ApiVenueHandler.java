package com.prcs251f.max.eventssystem;

import android.content.res.Resources;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by max on 09/05/2016.
 */
public class ApiVenueHandler extends AsyncTask<String, Void, Venue> {


    public ApiVenueHandler() {
        //Empty public constructor
    }

    @Override
    protected Venue doInBackground(String... params) {
        String foundParams = params[0];
        String venueId = "";

        if(params.length > 1) {
            venueId = params[1];
        }

        Venue retrievedVenue = new Venue();
        switch (foundParams) {
            case "getVenue":
                retrievedVenue = getVenueById(venueId);
                break;
        }
        return retrievedVenue;
    }

    /**
     * Search venue by id Method
     */
    private Venue getVenueById (String venueId) {
        Venue retrievedVenue = new Venue();
        try {
            URL url = new URL("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/venue?id=" + venueId);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            JSONObject jo = new JSONObject(new String(bufferedReader.readLine()));

            retrievedVenue.setVenueId(jo.getString("VENUEID"));
            retrievedVenue.setVenueName(jo.getString("VENUENAME"));
            retrievedVenue.setPostCode(jo.getString("POSTCODE"));
            retrievedVenue.setAddressLine1(jo.getString("ADDRESSLINE1"));
            retrievedVenue.setMaximumCapacity(jo.getString("MAXIMUMCAPACITY"));
            retrievedVenue.setStandingSpaces(jo.getString("STANDINGSPACES"));
            retrievedVenue.setSeatingSpaces(jo.getString("SEATINGSPACES"));

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return retrievedVenue;
    }
}
